from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import QuestionCategory, User, Comment, Question, QuestionAnswer, Profile
from .forms import NewQuestionForm, NewCommentForm, NewAnswerForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import permission_required


# Create your views here.
def index(request):
    num_questions = Question.objects.all().count()
    num_questions_answers = QuestionAnswer.objects.all().count()
    return render(
        request,
        'index.html',
        context={
            'num_questions': num_questions,
            'num_questions_answers': num_questions_answers,
        }
    )


class QuestionView(generic.ListView):
    model = Question
    context_object_name = 'question_list'
    queryset = Question.objects.all()
    ordering = ['category']
    paginate_by = 10
    template_name = 'question_list.html'


class QuestionDetailView(generic.DetailView):
    model = Question
    template_name = 'question_detail.html'

    def get_context_data(self, **kwargs):
        context = super(QuestionDetailView, self).get_context_data(**kwargs)
        form = NewCommentForm()
        context['comment_list'] = Comment.objects.filter(question_id=self.object.id)
        context['form'] = form
        return context


class QuestionAnswerListView(generic.ListView):
    model = QuestionAnswer
    context_object_name = 'question_answer_list'
    queryset = QuestionAnswer.objects.all()
    paginate_by = 10
    template_name = 'question_answer_list.html'


@permission_required('can_create_question')
def create_new_question(request):
    question = Question()
    if request.method == 'POST':
        form = NewQuestionForm(request.POST)
        if form.is_valid():
            question.questionText = form.cleaned_data['questionText']
            question.category = form.cleaned_data['category']
            question.save()
            return HttpResponseRedirect(reverse('question-list'))
    else:
        form = NewQuestionForm()

    return render(request, 'create_question.html', {'form': form})


def search_question(request):
    return render(request, 'search_question.html')


@permission_required('can_create_comment')
def create_new_comment(request, pk):
    question = get_object_or_404(Question, pk=pk)
    comment = Comment()
    if request.method == 'POST':
        form = NewCommentForm(request.POST)
        if form.is_valid():
            comment.commentText = form.cleaned_data['commentText']
            comment.author = Profile(request.user.id)
            comment.question = question
            comment.save()
            return HttpResponseRedirect(reverse('question-detail', args=[question.id]))
    else:
        form = NewCommentForm()

    return render(request, 'create_comment.html', {'form': form})


@permission_required('can_create_answer')
def create_new_answer(request, pk):
    question = get_object_or_404(Question, pk=pk)
    answer = QuestionAnswer()
    if request.method == 'POST':
        form = NewAnswerForm(request.POST)
        if form.is_valid():
            answer.answerText = form.cleaned_data['answerText']
            answer.question = question
            answer.save()
            return HttpResponseRedirect(reverse('question-answer'))
    else:
        form = NewAnswerForm()

    return render(request, 'create_answer.html', {'form': form})


class SearchResultListView(generic.ListView):
    model = Question
    template_name = 'question_list.html'
    context_object_name = 'question_list'

    def get_queryset(self):
        query = self.request.GET.get('q')
        question_list = Question.objects.filter(questionText__icontains=query)
        return question_list

