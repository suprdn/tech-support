from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from .models import Question, Comment, QuestionAnswer


class NewQuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ('questionText', 'category')


class NewCommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ('commentText',)


class NewAnswerForm(ModelForm):
    class Meta:
        model = QuestionAnswer
        fields = ('answerText',)