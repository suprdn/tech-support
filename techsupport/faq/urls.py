from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'questions/$', views.QuestionView.as_view(), name='question-list'),
    re_path(r'^question/(?P<pk>\d+)$', views.QuestionDetailView.as_view(), name='question-detail'),
    re_path(r'faq/$', views.QuestionAnswerListView.as_view(), name='question-answer'),
    re_path(r'create/$', views.create_new_question, name='create-question'),
    re_path(r'search/$', views.search_question, name='search-question'),
    re_path(r'search-res/$', views.SearchResultListView.as_view(), name='search-result'),
    re_path(r'question/(?P<pk>[-\w]+)/newcomment/$', views.create_new_comment,  name='create-comment'),
    re_path(r'question/(?P<pk>[-\w]+)/newanswer/$', views.create_new_answer,  name='create-answer'),
]