from django.contrib import admin
from .models import QuestionCategory, Profile, Comment, Question, QuestionAnswer

# Register your models here.
admin.site.register(QuestionCategory)
admin.site.register(Profile)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('author', 'question', 'commentText')


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('questionText', 'category')


@admin.register(QuestionAnswer)
class QuestionAnswerAdmin(admin.ModelAdmin):
    list_display = ('question', 'answerText')