from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.
class QuestionCategory(models.Model):
    name = models.CharField(
        max_length=30,
        verbose_name='Категория вопроса',
        help_text='Введите категорию вопроса',
    )

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Question(models.Model):
    category = models.ForeignKey(
        'QuestionCategory',
        verbose_name='Категория вопроса',
        on_delete=models.SET_NULL,
        null=True
    )
    questionText = models.TextField(
        max_length=500,
        verbose_name='Текст вопроса',
        help_text='Введите текст вопроса',
    )
    isOpen = models.BooleanField(
        verbose_name='Статус',
        default=0,
        help_text='Открыт вопрос или закрыт',
    )

    class Meta:
        permissions = (("can_create_question", "Create new question"),)

    def __str__(self):
        return self.questionText

    def get_absolute_url(self):
        return reverse('question-detail', args=[str(self.id), ])


class Comment(models.Model):
    author = models.ForeignKey(
        'Profile',
        verbose_name='Пользователь',
        on_delete=models.SET_NULL,
        null=True
    )
    question = models.ForeignKey(
        'Question',
        verbose_name='Вопрос',
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    commentText = models.TextField(
        max_length=500,
        verbose_name='Текст комментария',
        help_text='Введите комментарий',
    )

    class Meta:
        permissions = (('can_create_comment', "Create new comment"),)

    def __str__(self):
        return self.commentText


class QuestionAnswer(models.Model):
    question = models.ForeignKey(
        'Question',
        verbose_name='Вопрос',
        on_delete=models.SET_NULL,
        null=True
    )
    answerText = models.TextField(
        max_length=500,
        verbose_name='Ответ на вопрос',
        help_text='Введите ответ на вопрос'
    )

    class Meta:
        permissions = (("can_set_answer", "Set answer for question"),)

    def __str__(self):
        return self.answerText

    def get_absolute_url(self):
        return reverse('question-answer', args=[str(self.id), ])

